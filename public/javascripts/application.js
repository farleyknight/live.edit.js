window.editor = {};

(function($) {
  jQuery(document).ready(function() {
    window.editor = CodeMirror.fromTextArea($("#code")[0], {
      lineNumbers: true
    });
    
    /* @todo (farleyknight@gmail.com): Take this functionality and place it into a plugin */
    window.editor.onChangeDelay = null;
    var $previewWindow = window.open();

    var updatePreview = function() {
      $($previewWindow.document.body).html(window.editor.getValue());      
    };

    window.editor.on("change", function() {
      clearTimeout(window.editor.onChangeDelay);
      window.editor.onChangeDelay = setTimeout(updatePreview, 300);
    });

    setTimeout(updatePreview, 300);

    // @todo (farleyknight@gmail.com): Refactor this!
    var determineMode = function(fileName) {
      if (fileName.match(".html$") != null) {
        return "xml";
      } else if (fileName.match(".xml$") != null) {
        return "xml";
      } else if (fileName.match(".js$") != null) {
        return "javascript";
      } else {
        throw "Cannot determine file type!";
      }

      return null;
    };

    $.getJSON("/current/", function(data) {    
      window.editor.setOption("mode", determineMode(data["name"]));
      window.editor.setValue(data["body"]);
    });

    // @note Change theme
    $("#select").change(function() {
      var input = $("#select")[0];
      var theme = input.options[input.selectedIndex].innerHTML;
      window.editor.setOption("theme", theme);
      $(".nav .active a").css({
        color              : $(".cm-variable").css("color"),
        'background-color' : $(".CodeMirror").css("background-color")
      });
      $('.CodeMirror-gutters').css({
        'background-color' : $(".CodeMirror").css("background-color"),
        'box-shadow'       : "0px"
      });
    });

    // @note Close $previewWindow before unloading the current window
    $(window).unload(function() {
      $previewWindow.close();
    });
  });

})(jQuery);