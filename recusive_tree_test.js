/*
 * @note (farleyknight@gmail.com): My main point behind this file
 * was to write a recursive algorithm that grabbed the entire directory
 * tree and return it to the user as a JSON object.
 * 
 * Unfortunately I never finished it, but I imagine it will be useful at 
 * some point in the future.
 *  
 */

var fs   = require('fs');
var path = require('path');

var walk = function (start, callback) {
  fs.lstat(start, function (err, stat) {
    if (err) { return callback(err); }
    if (stat.isDirectory()) {

      fs.readdir(start, function (err, files) {
        var coll = files.reduce(function (acc, i) {
          var abspath = path.join(start, i);

          if (fs.statSync(abspath).isDirectory()) {
            walk(abspath, callback);
            acc.dirs.push(abspath);
          } else {
            acc.names.push(abspath);
          }

          return acc;
        }, {"names": [], "dirs": []});

        return callback(null, start, coll.dirs, coll.names);
      });
    } else {
      return callback(new Error("path: " + start + " is not a directory"));
    }
  });
};

/*
walk(".", function(err, start, dirs, names) {
  // console.log("error", err);
  // console.log("start", start);
  // console.log("dirs", dirs);
  // console.log("names", names);
});
*/

var walkSync = function (start, callback) {
  var stat = fs.statSync(start);

  if (stat.isDirectory()) {
    var filenames = fs.readdirSync(start);

    var coll = filenames.reduce(function (acc, name) {
      var abspath = path.join(start, name);

      if (fs.statSync(abspath).isDirectory()) {
        acc.dirs.push(name);
      } else {
        acc.names.push(name);
      }

      return acc;
    }, {"names": [], "dirs": []});

    callback(start, coll.dirs, coll.names);

    coll.dirs.forEach(function (d) {
      var abspath = path.join(start, d);
      walkSync(abspath, callback);
    });

  } else {
    throw new Error("path: " + start + " is not a directory");
  }
};


walkSync(".", function(err, start, dirs, names) {
  console.log("error", err);
  console.log("start", start);
  console.log("dirs", dirs);
  console.log("names", names);
});


