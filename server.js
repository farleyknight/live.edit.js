
/* File system */
var fs      = require("fs");
var express = require("express");
var app     = express();
var walk    = require('walk');
var path    = require('path');

/* MongoDB */
var mongo  = require('mongodb');
var Db     = mongo.Db;
var Server = mongo.Server;

var client = new Db('test', new Server('127.0.0.1', 27017), {safe: false});

/* node server.js usage */
var argv    = require('optimist')
      .usage('Usage: node server.js -p 8080 --app=$HOME/my/project/directory')
      .default({app: "app/", port: '8080'})
      .alias('p', 'port')
      .describe('p', 'The port number of the server, e.g. localhost:8080 (by default)')
      .alias('a', 'app')
      .describe('a', 'The application directory to launch the editor from. ("app/" by default)')
      .argv;


client.open(function(err, pClient) {
  client.collection('files').remove();
  client.collection('files', function(err, collection) {    
    collection.insert({body: fs.readFileSync("app/application.css").toString(), name: "application.css"});
    collection.insert({body: fs.readFileSync("app/index.html").toString(), name: "index.html"});
    collection.insert({body: fs.readFileSync("app/application.js").toString(), name: "application.js"});
  });
});

// Install simple logger
app.use(function(req, res, next){
  console.log('%s %s', req.method, req.url);
  next();
});

// Serve assets from public at root (/)
app.configure(function() {
  app.use('/', express.static('public'));
});

// Show all files for GET /files/
app.get("/files/", function(req, res, next) {
  client.collection('files', function(err, collection) {
    collection.find().toArray(function(err, results) {
      res.send(results);
    });
  });
});

// Allow app.param to take a name and a regex to
// match against.
/*
app.param(function(name, fn){
  if (fn instanceof RegExp) {
    return function(req, res, next, val){
      var captures = fn.exec(String(val));
      if (captures) {
        req.params[name] = captures;
        next();
      } else {
        next('route');
      }
    };
  }
});
*/

// Parse :id as 0 through 9 and 'a' up to 'f'
// This **should** match the same regex for 
// mongodb ids.
// app.param('id', /^[0-9a-f]+$/);

// Show one file for GET /files/32432432432
app.get("/files/:id", function(req, res, next) {
  console.log("Request params is", req.params);
  client.collection('files', function(err, collection) {
    console.log("Collection is", collection);
    collection.findOne({
      '_id' : new mongo.ObjectID(req.params.id)
    }, function(err, file) {
      console.log("Error for file is", err);
      console.log("File is", file);
      res.send(file);
    });
  });
});

app.get("/current/body/", function(req, res, next) {
  client.collection('files', function(err, collection) {
    collection.findOne({name: "index.html"}, function(err, file) {
      res.send(file["body"]);
    });
  });  
});

// Current always points to index.html in MongoDB
app.get("/current/", function(req, res, next) {
  client.collection('files', function(err, collection) {
    collection.findOne({name: "index.html"}, function(err, file) {
      res.send(file);
    });
  });
});

// Start up the server
app.listen(argv.port);

