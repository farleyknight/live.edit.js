
var mongo  = require('mongodb');
var Db     = mongo.Db;
var Server = mongo.Server;

var client = new Db('test', new Server('127.0.0.1', 27017, {}));

client.open(function(err, pClient) {
  client.collection('files', function(err, collection) {
    collection.insert({body: "really really long document", name: "something.css"});
    collection.insert({body: "really really long document", name: "something.html"});
    collection.insert({body: "really really long document", name: "something.js"});
  });
});

